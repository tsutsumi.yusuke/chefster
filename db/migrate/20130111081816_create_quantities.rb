class CreateQuantities < ActiveRecord::Migration
  def change
    create_table :quantities do |t|
      t.string :name
      t.string :singular
      t.string :plural

      t.timestamps
    end
  end
end
