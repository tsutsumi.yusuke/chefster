class Quantity < ActiveRecord::Base
  attr_accessible :name, :plural, :singular
end
